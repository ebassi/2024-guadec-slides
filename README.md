Slides for GUADEC 2024
======================

"Moving Past Quiet Introspection".

Viewing the slides
------------------

The slides deck is rendered by [reveal.js](https://revealjs.com/) from a
MarkDown file.

Enter `introspection` and run a local web server, for instance:

```shell
python3 -m http.server 8000
```

Then point your web browser to `http://localhost:8000` to view the slides.

Copyright and licensing
-----------------------

The Igalia slides template is copyright Igalia S.L.
