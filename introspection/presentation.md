<!-- .slide: data-cover -->

<h1 id="presentation-title">Moving Past Quiet Introspection</h1>
<h5 id="presentation-author">Emmanuele Bassi (he/him)</h5>
<h5 id="presentation-date">2024-07-20</h5>

---

## Disclaimer

Note:
This presentation is about the changes in gobject-introspection as a
project, and what those changes mean to library maintainers. Philip Withnall
has a presentation tomorrow regarding the move of libgirepository into GLib,
and what that means to consumers of GLib, language binding developers, as
well as distributors, so make sure you don't miss that one.

---

## Definition

**Introspection**, noun:

- (object-oriented programming) The ability of a program to examine at run
  time the type or properties of an object

Note:
Let's start with a simple dictionary definition, so that we are all on the
same page. Introspection is a fairly well established concept in object
oriented programming. Of course, since GNOME has its own flavour of object
orientation, it also needs its own flavour of introspection.

---

## GObject-introspection

> a machine readable description of a C ABI based on the run time type
> system of GObject (GType), built by parsing C declarations and document
> blocks; the description can be consumed to call functions, generate code,
> or generate documentation.

Note:
Whenever we talk about the GNOME application development platform, we
constantly remind people that they can write their applications in a variety
of languages, not just C. It is an important feature of our project that we
cater to many other communities, and that we provide an environment that
does not force you to learn a language as well as a platform at the same
time. Of course, it would be easier if we only supported a single language,
just like we have a single toolkit, a single theme, a single set of design
principles; but, at the end of the day, GNOME unfortunately does not have
the kind of pull to get everyone to switch to Rust for everything, cool as
that might be. In order to consume our stack in different programming
languages we need to settle on a shared format that is capable of accurately
describing not only the C ABI that the stack provides, but also the
semantics of types and functions.

---

## GObject-introspection

> a machine readable description of **a C ABI** based on the run time type
> system of GObject (GType), built by parsing C declarations and document
> blocks

Note:

An important side note: GObject-introspection is not the description of a C
API. It does not describe what the header file of a library contains. In
many cases, it doesn't even describe what a source file contains. It is not
something that takes C code, turns it into XML, and from the XML generates
other C code: it is not an interface definition language.

GObject-introspection describes a C ABI: the symbols and identifiers that
can be used to find functions inside a shared library, and wrap native values
into data that can be passed at run time across the foreign-function
interface (FFI) boundary. You have to understand what a C ABI is, if you
want to play this game.

---

## Formats

- GIR (XML)
- Typelib

Note:
Introspection data comes in two variants: an XML document and a binary
format. The XML document is expansive, and it's generally used for
generating code or documentation. The binary format is more efficient, but
the conversion from XML is lossy, so it's really useful for bindings to
dynamic languages. The important part is that both the GIR and the typelib
data are platform and architecture dependent. They can only be used on the
same platform and architecture that generated them. GIR files can be
architecture independent, but there's no real guarantee for that: the
maintainer of the library needs to know whether it applies or not. For people
writing GIR parsers there is a RelaxNG schema in the gobject-introspection
repository: make sure to use it.

---

## The Land Before Introspection

- Every language for itself
- Hand writing for (little) fun and (no) profit
- S-expr
- Code generation
- Overrides

Note:
The GTK binding community has existed just as long as GTK. Everything started
off with hand written wrappers that called into native functions. Given
enough time, programmers will try to program themselves out of any issue,
so we got progressively more intricate code generators, and tools that
parsed C declarations to generate structured data that could be turned into
static bindings. Since we did not have a decent way to describe the
semantics of the C ABI, every binding had its own way of overriding the code
generator, using some variant of C code that would be compiled alongside the
rest. Each library would have its own separate binding, maintained as a
separate project, and every language binding would have its own
interpretation of the semantics of each library.

---

## Introducing introspection / 1

- Shared format
- Shared semantics
- Keep the description of the library close to the library

Note:
Instead of having a bestiary of formats, and a veritable cornucopia of small
modules, we now have a shared data format that describes not just an ABI, but
also the semantics of the ABI; and, for good measure, the data is built by
the library that provides the ABI, so we only need to build it once.

---

## Introducing introspection / 2

- Hard to add on extant code bases
- Syntax and defaults need finalization
- Iterative approach

Note:
Of course, there are downsides. Introspection is generally hard to introduce
after the fact, and onto an existing, unsuspecting, code base, especially
one as complex as the whole GNOME application development stack; the initial
decision was to use an iterative approach, in order to try out the various
annotations, their syntax, and their default behaviour, without requiring a
flag day to turn it on, or a massive, coordinated ABI break to fix all the
issues. This meant codifying the existing practices and behaviours of
various libraries, and slowly incentivise the maintainers to replace bits
that did not conform.

---

## Perks of being a wallflower

- Encode existing practices as defaults
  - ownership transfer
  - callbacks scope and closure
- Quiet output

Note:
To be fair, this was the most reasonable approach at the time; it allowed
gradual implementation of the introspection facilities, while leaving space
for a day where every library followed the established well practices of the
project.

---

![](./introspection-plan.jpg)

Note:
Of course, no plan survives contact with enemy. It turns out that
documentation is the ever present gotcha for all free and open source
software, and introspection is not an exception. Everybody was on board for
the first part of the roadmap, but nobody took the second part—the part
where every library would end up with an enforced conformity—seriously, or
planned for it.

---

## Introspection has been too quiet

- Easy to ignore recommendations
- Easy to ignore warnings
- Easy to ignore errors

Note:
This initial decision mandated a very low friction approach: rely on
heuristics *a lot*, for everything; emit warnings only on demand; error out
only in the most dire situations. Since programmers are incredibly lazy,
this has created a lot of bad incentives, like disabling introspection by
default; or making it silent; or just ignoring all output coming from the
introspection tooling. This has to change. This will change.

---

## Turning up the volume

Note:
The time has finally come for the second part of the plan. For that, we will
have three, easy to follow, steps based on a stereotypically British scale.

---

## Step 1: Tut-tutting

- Emit better diagnostics
  - location of undefined behaviour
  - possible solutions
  - use color and links
  - "clippy" mode
- Add more warnings
  - heuristics → diagnostic messages

Note:
The first step is based on improving the existing warning infrastructure,
following the trajectory of existing C compilers and their diagnostic
messages. After all, if C programmers have been able to cope with the
warnings coming from clang and GCC with every toolchain update, they can
also deal with an increase in warnings coming from the introspection
tools. Ideally, we should have a proper repository of errors and examples of
bad patterns, and how to fix them, because apparently that's how grown-ups
do things.

---

## Step 2: Polite coughing

- Enable fatal warnings
  - `fatal_warnings: get_option('werror')`
  - needs cooperation from the build system
  - improve Meson's `gnome.generate_gir()`
- Fail early in CI
  - enable `--werror`, please

Note:
The second step requires some help from the maintainers and, possibly, the
build system. Ideally, maintainers should already enable fatal compiler
warnings when running a CI pipeline, so enabling them for the introspection
scanner would not be a major change. Additionally, Meson could do that for
us automatically, just like it can pass the appropriate compiler argument
when using its own error option.

---

## Step 3: Glassing

- Strict mode
  - `g-ir-scanner --strict`
  - heuristics → warnings
  - defaults → warnings
  - backward compatibility is not required
- Drop poorly defined heuristics
  - best practices need to be enforced

Note:

This is where the escalation in enforcement should become self-evident. The
strict mode was added in 2022 to handle the case of colliding properties,
signals, and virtual functions, something that made the Vala programming
language very sad. Strict mode is still opt-in, but for those who want to
increase the level of strictness, it will soon expose much more: for
instance, it will emit notices for heuristics and default values. It will
also add more cases where the API, while perfectly valid for C, may
introduce invalid or non-idiomatic constructs in well known language
bindings; for instance, enumeration fields starting with a number after
tokenisation, or collisions across interface methods. These notices can be
collected into a report at the end of the build, and maintainers will be
able to use them when changing their API.

---

## Heuristics & Defaults / 1

> So you need another heuristic to handle that, and of course "heuristic" is
> an ancient african word meaning "maybe bonghits will make this problem
> more tractable". 

Matthew Garrett

_Re: Why EDID is not trustworthy for DPI_

Note:
We talked about heuristics and defaults used to introduce introspection into
the existing GNOME stack, but what are those? They are generally documented
on the gobject-introspection website, but not everyone is aware of them,
which means things seem to work until they don't, and then people don't
understand how to fix them when the CI pipeline starts screaming at you.

For the curious: yes, this is from the same thread that gave you "is linux
about choice". 

---

## Heuristcs & Defaults / 2

- ownership transfer
  - none, (floating), full
  - caller-allocates, callee-allocates
  - nullable, optional
- array
  - element-type, zero-terminated

Note:
Ownership transfer is probably the most important part of the introspection
data; without it, we either get leaks or we get crashes, because the C type
system, such as it is, does not tell you anything about the lifetime of a
type, or the semantics of moving pointers around: it's all just memory
addresses. The default, by and large, is "no ownership transfer" of
pointers, unless it's a constructor, in which case it's either a full
transfer, or a floating one in case you're using `GInitiallyUnowned`, which
you should just not do. Automatic full ownership for `out` arguments is
reserved for callee-allocated one, for obvious reasons. All `gpointer` types
are nullable by default, otherwise nullability is strictly manual.
Similarly, all arrays have the element type of their argument and are
zero-terminated unless otherwise specified.

---

## Heuristics & Defaults / 3

- callbacks
  - scope, closure, destroy
- constructors
- methods
- `GError`
- `GAsyncReadyCallback`, `GCancellable`

Note:
Callbacks have proven to be really hard to understand—I fell into the trap
myself multiple times, until I decided to spelunk into the introspection
scanner code base. All annotations for a callback go on the callback
argument. Scope defines the scope of the closure; closure points to the
argument that contains the user data passed to the callback; and destroy
points to the argument with the notification function for the `notified`
scope. The only place where you should use a `closure` annotation with no
argument is on the user data parameter of the callback's typedef. Constructors
must end with, or contain the token `new` in the symbol name, otherwise they
need a constructor annotation. GError arguments are removed from the
list of parameters, and the callable is marked as "throws".
GAsyncReadyCallback and GCancellable arguments are automatically nullable,
unless they are marked as `out`.

---

## Documentation

- structured types
  - copy-func, free-func
- functions
  - async-func, sync-func, finish-func
  - set-property, get-property
- properties
  - setter, getter
- signals
  - emitter
- deprecation, stability
- attributes

Note:

The XML variant of the introspection data contains all the documentation
associated to each symbol or identifier, as best as it could be collected.
Each doc block is paired to a declaration, so any doc block for a private
symbol will be ignored. Some additional annotations are used to generate the
appropriate links or blurbs in documentation generators, like the property
accessors, or the signal emitter, or the function to call at the end of a
GAsyncReadyCallback. Deprecation and stability tags are also consumed by
documentation generators. Attributes are free-form key/value pairs that can
be useful to extend the grammar of the introspection data without
necessarily adding more annotations.

---

## Traps

- Pre-processor macros
- inline functions
- GLib container data types

Note:
C programmers have the tendency to expose a bunch of API entry points using
macros, because when you don't have templates or generic types, everything
looks like text substitution. This stuff does **not** exist at the ABI
level, so forget about it. Provide actual types and functions for everybody
else. You can expose inline functions, but, again, those are not in the ABI,
so you should always have a fallback. The introspection data will include
static inlines, as long as you provide a declaration, and not just a
definition. Finally, stop using GList, GSList, GArray, GPtrArray,
GByteArray, and GHashTable at the API boundary; those are for
implementations only, and you should never expose them to users.

---

## Changes

- 1.80: include static inline symbols in GIR
- 1.80: async/sync/finish functions
- 1.80: vfunc and callback fields doc blocks detection
- 1.82: validate enum members
- 1.82: support `G_TYPE_POINTER` types
- 1.82: support more integer-sized top-level types

---

## Planned Changes

- split test suite to its own repository
- move to GType inspection via `dlopen`
- more warnings/better strict mode

---

## Recap

- Louder introspection
- Do not rely on heuristics and defaults
- Document everything

Note:
Introspection is going to get louder so people need to start taking it seriously.

Do not rely on heuristics and defaults, unless you understand them.

Write documentation like your users depend on it, because they do, and in
more languages than you can expect.

---

## Links in bio

- https://gitlab.gnome.org/GNOME/gobject-introspection/
- https://gi.readthedocs.io/en/latest/
- https://docs.gtk.org/girepository/

---

## Thank you

- Like
- Comment
- Subscribe
