const pkg = require('./package.json')
const yargs = require('yargs')
const through = require('through2');

const sass = require('sass')

const gulp = require('gulp')
const connect = require('gulp-connect')

const root = yargs.argv.root || '.'
const port = yargs.argv.port || 8000
const host = yargs.argv.host || 'localhost'

// Prevents warnings from opening too many test pages
process.setMaxListeners(20);

// a custom pipeable step to transform Sass to CSS
function compileSass() {
    return through.obj( ( vinylFile, encoding, callback ) => {
      const transformedFile = vinylFile.clone();

      sass.render({
          data: transformedFile.contents.toString(),
          includePaths: ['css/', 'css/theme/template']
      }, ( err, result ) => {
          if( err ) {
              console.log( vinylFile.path );
              console.log( err.formatted );
          }
          else {
              transformedFile.extname = '.css';
              transformedFile.contents = result.css;
              callback( null, transformedFile );
          }
      });
    });
  }

gulp.task('css-themes', () => gulp.src(['./css/theme/source/*.{sass,scss}'])
        .pipe(compileSass())
        .pipe(gulp.dest('./css/theme'))
);

gulp.task('css-reveal-core', ()=> gulp.src(['./node_modules/reveal.js/dist/*.css'])
        .pipe(gulp.dest('./css'))
);

gulp.task('css-reveal-themes', ()=> gulp.src(['./node_modules/reveal.js/dist/theme/**/*'])
    .pipe(gulp.dest('./css/theme'))
);

gulp.task('css-reveal-mixins', ()=> gulp.src(['./node_modules/reveal.js/css/theme/template/mixins.scss',
'./node_modules/reveal.js/css/theme/template/settings.scss'])
    .pipe(gulp.dest('./css'))
)

gulp.task('css-reveal', gulp.parallel('css-reveal-core', 'css-reveal-themes', 'css-reveal-mixins'))

gulp.task('css', gulp.parallel('css-themes'))

gulp.task('js',async () => gulp.src(['./node_modules/reveal.js/dist/*.{js,map}'])
        .pipe(gulp.dest('./js'))
);

gulp.task('plugins', async () => gulp.src(['./node_modules/reveal.js/plugin/**/*'])
        .pipe(gulp.dest('./plugin'))
);

gulp.task('default', gulp.series('css-reveal', gulp.parallel('css', 'js', 'plugins')))

gulp.task('build', gulp.series('css-reveal', gulp.parallel('css', 'js', 'plugins')))

gulp.task('reload', () => gulp.src(['*.html', '*.md'])
    .pipe(connect.reload()));

    gulp.task('serve', () => {

        connect.server({
            root: root,
            port: port,
            host: host,
            livereload: true
        })

        gulp.watch(['*.html', '*.md'], gulp.series('reload'))

        gulp.watch(['plugin/**/plugin.js', 'plugin/**/*.html'], gulp.series('plugins', 'reload'))

        gulp.watch([
            'css/theme/source/*.{sass,scss}',
            'css/theme/template/*.{sass,scss}',
        ], gulp.series('css-themes', 'reload'))


    })